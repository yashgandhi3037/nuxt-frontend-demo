module.exports = {
  purge: [],
  darkMode: false,
  theme: {
    extend: {
      colors: {
        blue: {
          50: '#F7FAFC',
          150: '#EBF4FF',
          350: '#81E6D9',
          450: '#319795',
          650: '#3182CE',
        },
        gray: {
          250: '#CCCCCC',
          350: '#D6D6D6',
          450: '#707070',
          550: '#718096',
          650: '#4A5568',
          750: '#2D3748',
        },
        green: {
          150: '#E6FFFA',
          250: '#81E6D9',
        },
        indigo: {
          250: '#CBD5E0',
        },
      },
      fontFamily: {
        sans: ['Lato', 'sans-serif'],
      },
      fontSize: {
        '5xl': '2.625rem',
      },
      lineHeight: {
        'leading-11': '3.125rem',
        'leading-12': '9.75rem',
      },
    },
  },
  variants: {
    extend: {
      textColor: ['active'],
    },
  },
  plugins: [],
}
